import nltk
from nltk.stem import WordNetLemmatizer

lemmatizer_output = WordNetLemmatizer()
print(lemmatizer_output.lemmatize('working'))
